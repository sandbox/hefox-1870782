<?php

function _spaces_og_book_add_form_elements(&$form, $node, $group) {

  // Need this for AJAX.
  $form['#cache'] = TRUE;
  drupal_add_js("if (Drupal.jsEnabled) { $(document).ready(function() { $('#edit-book-pick-book').css('display', 'none'); }); }", 'inline');

  $form['book'] = array(
    '#type' => 'fieldset',
    '#title' => t('Book outline'),
    '#weight' => 10,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => TRUE,
    '#attributes' => array('class' => 'book-outline-form'),
  );
  foreach (array('menu_name', 'mlid', 'nid', 'router_path', 'has_children', 'options', 'module', 'original_bid', 'parent_depth_limit') as $key) {
    $form['book'][$key] = array(
      '#type' => 'value',
      '#value' => isset($node->book[$key]) ? $node->book[$key] : NULL,
    );
  }

  $form['book']['plid'] = _book_parent_select($node->book);

  $form['book']['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#default_value' => $node->book['weight'],
    '#delta' => 15,
    '#weight' => 5,
    '#description' => t('Pages at a given level are ordered first by weight and then by title.'),
  );
  $options = array();
  $nid = isset($node->nid) ? $node->nid : 'new';

  if (isset($node->nid) && ($nid == $node->book['original_bid']) && ($node->book['parent_depth_limit'] == 0)) {
    // This is the top level node in a maximum depth book and thus cannot be moved.
    $options[$node->nid] = $node->title;
  }
  else {
    foreach (spaces_og_book_get_books($group) as $book) {
      $options[$book['nid']] = $book['title'];
    }
  }
  if (user_access('create new books') && ($nid == 'new' || ($nid != $node->book['original_bid']))) {
    // The node can become a new book, if it is not one already.
    $options = array($nid => '<'. t('create a new book') .'>') + $options;
  }
  if (!$node->book['mlid']) {
    // The node is not currently in a the hierarchy.
    $options = array(0 => '<'. t('none') .'>') + $options;
  }

  // Add a drop-down to select the destination book.
  $form['book']['bid'] = array(
    '#type' => 'select',
    '#title' => t('Book'),
    '#default_value' => $node->book['bid'],
    '#options' => $options,
    '#access' => (bool)$options,
    '#description' => t('Your page will be a part of the selected book.'),
    '#weight' => -5,
    '#attributes' => array('class' => 'book-title-select'),
    '#ahah' => array(
      'path' => 'book/js/form',
      'wrapper' => 'edit-book-plid-wrapper',
      'effect' => 'slide',
    ),
  );
  $form['book']['pick-book'] = array(
    '#type' => 'submit',
    '#value' => t('Change book (update list of parents)'),
     // Submit the node form so the parent select options get updated.
     // This is typically only used when JS is disabled.  Since the parent options
     // won't be changed via AJAX, a button is provided in the node form to submit
     // the form and generate options in the parent select corresponding to the
     // selected book.  This is similar to what happens during a node preview.
    '#submit' => array('node_form_submit_build_node'),
    '#weight' => 20,
  );
}


/**
 * Returns an array of all books.
 *
 * This list may be used for generating a list of all the books, or for building
 * the options for a form select.
 */
function spaces_og_book_get_books($group) {
  static $all_books = array();

  if (!isset($all_books[$group->nid])) {
    $all_books[$group->nid] = array();
    $result = db_query("SELECT DISTINCT(bid) FROM {book} b INNER JOIN {og_ancestry} o ON o.nid = b.nid WHERE o.group_nid = %d", $group->nid);
    $nids = array();
    while ($book = db_fetch_array($result)) {
      $nids[] = $book['bid'];
    }
    if ($nids) {
      $result2 = db_query(db_rewrite_sql("SELECT n.type, n.title, b.*, ml.* FROM {book} b INNER JOIN {node} n on b.nid = n.nid INNER JOIN {menu_links} ml ON b.mlid = ml.mlid WHERE n.nid IN (". implode(',', $nids) .") AND n.status = 1 ORDER BY ml.weight, ml.link_title"));
      while ($link = db_fetch_array($result2)) {
        $link['href'] = $link['link_path'];
        $link['options'] = unserialize($link['options']);
        $all_books[$group->nid][$link['bid']] = $link;
      }
    }
  }
  return $all_books[$group->nid];
}